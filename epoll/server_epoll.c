#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include <sys/epoll.h>

#define MAX_CON (1200)

int main(int argc, char *argv[])
{
	int listenfd = 0, connfd = 0;
	struct sockaddr_in serv_addr; 

	char sendBuff[1025];
	time_t ticks; 

	int res = -1;

	int epfd = -1, i = 0, nop;

	static struct epoll_event *events;
	struct epoll_event ev;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);

	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &nop, sizeof(int));

	memset(&serv_addr, '0', sizeof(serv_addr));
	memset(sendBuff, '0', sizeof(sendBuff)); 

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000); 		// port : 5000

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

	listen(listenfd, 10); 

	// epoll
	events = calloc(MAX_CON, sizeof(struct epoll_event));
	if((epfd = epoll_create(MAX_CON)) == -1)
	{
		perror("epoll_create");
		exit(1);
	}

	ev.events = EPOLLIN;
	ev.data.fd = listenfd; // fdmax
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, listenfd, &ev) < 0) 
	{
		perror("epoll_ctl");
		exit(1);
	}

	while(1)
	{
		res = epoll_wait(epfd, events, MAX_CON, -1);		// wait till listen socket can read without block
		connfd = events[i].data.fd;

		// check all connection ( ith connection )
		for(i=0; i<MAX_CON; i++){
			if(connfd == listenfd)
			{	// accept
				if((connfd = accept(listenfd, (struct sockaddr*)NULL, NULL)) == -1)
				{
					perror("Server accept\n");
				}else
				{
					ev.events = EPOLLOUT;
					ev.data.fd = connfd;
					if(epoll_ctl(epfd, EPOLL_CTL_ADD, connfd, &ev) < 0){
						perror("epoll_ctl");
						exit(1);
					}
					break;	// important!
				}
			}else
			{	// send current time
				if(events[i].events & EPOLLHUP)
				{
					if(epoll_ctl(epfd, EPOLL_CTL_DEL, connfd, &ev) < 0){
						perror("epoll_ctl");
					}
					close(connfd);
					break;
				}
				if(events[i].events & EPOLLOUT)				// connect socket can write without block
				{
					ticks = time(NULL);
					snprintf(sendBuff, sizeof(sendBuff), "%.24s\r\n", ctime(&ticks));
					// printf("write time!\n");
					write(connfd, sendBuff, strlen(sendBuff)); 

					close(connfd);
					//printf("start sleep\n");
					//sleep(1);		// sleep 1 second
					//printf("end sleep\n");
					break;
				}
			}
		}

	}
}
